from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from . import endpoints

router = DefaultRouter()
router.register(r"register", endpoints.UserRegisterViewSet)
router.register(r"posts", endpoints.PostViewSet)


urlpatterns = [
    url(r"^auth/$", endpoints.ObtainJSONWebToken.as_view()),
    url(r"^", include(router.urls)),
]
