from django.conf import settings
from django.db import models


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="posts")
    title = models.CharField(max_length=256)
    content = models.TextField()

    likes = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   related_name="likes")

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s by user #%s" % (self.title, self.user_id)

    def add_like(self, user):
        self.likes.add(user)

    def remove_like(self, user):
        self.likes.remove(user)
