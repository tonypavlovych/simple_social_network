import json

import clearbit
import logging
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from requests import HTTPError

from .utils import get_clearbit_data

logger = logging.getLogger(__name__)

clearbit.key = settings.CLEARBIT_KEY


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def get_clearbit_info(*, instance, created=False, **kwargs):
    """
    Fetches additional data for users using Clearbit Enrichment API.
    """
    if created:
        try:
            clearbit_data = get_clearbit_data(instance.email)
        except HTTPError as e:
            logger.error("Got HTTP Error while retrieving "
                         "Clearbit data for user #{}\n{}"
                         .format(instance.pk, e))
        else:
            logger.info("Successfully fetched Clearbit data for user #{}"
                        .format(instance.pk))
            instance.clearbit_data = json.dumps(clearbit_data, indent=4)
            instance.save(update_fields=["clearbit_data", ])
