# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-10 14:04
from __future__ import unicode_literals

import apps.accounts.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('email', models.EmailField(max_length=255, unique=True, validators=[apps.accounts.validators.EmailHunterValidator()], verbose_name='email address')),
                ('clearbit_data', models.TextField(blank=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
